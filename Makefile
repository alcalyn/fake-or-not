.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-16s\033[0m %s\n", $$1, $$2}'

.PHONY: start
start: up info serve ## Start development environemnt

.PHONY: init
init: up install start ## Init development environment (after git clone)

.PHONY: node
node: ## Enters into Node container
	docker-compose exec node bash

.PHONY: up
up: ## Start docker containers
	docker-compose up -d

.PHONY: install
install: ## Install development environment
	docker-compose exec node bash -c "yarn install"

.PHONY: deploy
deploy: ## Rebuild prod application
	docker-compose exec node bash -c "yarn install"
	docker-compose exec node bash -c "yarn build"
	docker-compose restart

.PHONY: serve
serve: ## Serve dev environment
	docker-compose exec node bash -c "yarn install"

.PHONY: info
info: ## Display
	#
	# Dev url:  http://0.0.0.0:19100
	# Prod url: http://0.0.0.0:19101 (Run "make deploy" first)
	#
