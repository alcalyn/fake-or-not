import Vue from 'vue';

Vue.filter('percent', (number: number) => {
    return Math.round(number * 100) + ' %';
});
