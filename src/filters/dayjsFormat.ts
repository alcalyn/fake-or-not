import Vue from 'vue';
import dayjs from 'dayjs';
import 'dayjs/locale/fr';

Vue.filter('dayjsFormat', (date: string|Date, format: string, locale: string = 'fr') => {
    return dayjs(date)
        .locale(locale)
        .format(format)
    ;
});
