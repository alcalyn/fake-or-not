
import 'reflect-metadata';
import Vue from 'vue';
import VueMatomo from 'vue-matomo';
import BootstrapVue from 'bootstrap-vue';
import '@/filters/percent';
import '@/filters/dayjsFormat';
import App from '@/App.vue';
import router from '@/router';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.config.productionTip = false;

Vue.use(BootstrapVue);

if ('false' !== process.env.VUE_APP_MATOMO_ENABLED) {
    Vue.use(VueMatomo, {
        host: process.env.VUE_APP_MATOMO_HOST,
        siteId: process.env.VUE_APP_MATOMO_SITE_ID,
        debug: 'false' !== process.env.VUE_APP_MATOMO_DEBUG,
        trackInitialView: false, // False to prevent double track on first page
        router,
    });
}

new Vue({
    router,
    render: h => h(App),
}).$mount('#app');
