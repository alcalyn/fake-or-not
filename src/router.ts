import Vue from 'vue';
import Router from 'vue-router';
import GorafiOrNot from '@/views/GorafiOrNot.vue';
import Contribute from '@/views/Contribute.vue';
import Export from '@/views/Export.vue';
import Stats from '@/views/Stats.vue';

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: GorafiOrNot,
        },
        {
            path: '/contribute',
            name: 'contribute',
            component: Contribute,
        },
        {
            path: '/export',
            name: 'export',
            component: Export,
        },
        {
            path: '/stats',
            name: 'stats',
            component: Stats,
        },
    ],
});
