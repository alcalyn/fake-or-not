import { Container } from 'inversify';
import Api from '@/services/Api';
import TrackingInterface from '@/services/Tracking/TrackingInterface';
import NullTracking from '@/services/Tracking/NullTracking';
import MatomoTracking from '@/services/Tracking/MatomoTracking';
import Export from '@/views/Export.vue';

var container = new Container();

container.bind<Api>('Api').to(Api);
container.bind<Export>('Export').to(Export);

if ('false' !== process.env.VUE_APP_MATOMO_ENABLED) {
    container.bind<TrackingInterface>('TrackingInterface').to(MatomoTracking);
} else {
    container.bind<TrackingInterface>('TrackingInterface').to(NullTracking);
}

export default container;
