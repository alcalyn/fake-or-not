import Article from '@/models/Article';

export default class extends Article {
    answered: number = NaN;
    notAnswered: number = NaN;
    total: number = NaN;
    ratio: number = NaN;
}
