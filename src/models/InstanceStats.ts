export default class {
    count: {
        total: number,
        real: number,
        fake: number,
    } = {
        total: 0,
        real: 0,
        fake: 0,
    };
}
