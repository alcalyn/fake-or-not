export default class {
    articleIds: number[] = [];
    answer: number|null = null;

    constructor(articleIds: number[], answer: number) {
        this.articleIds = articleIds;
        this.answer = answer;
    }
}
