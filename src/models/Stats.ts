import ArticleWithStats from './ArticleWithStats';

export default class {
    statsGeneratedAt: Date = new Date();
    fakeThatSeemsReals: ArticleWithStats[] = [];
    realThatSeemsFakes: ArticleWithStats[] = [];
}
