export default class {
    id: number = 0;
    url: string = '';
    title: string = '';
    fake: boolean = false;
    datePublished: string = '';
}
