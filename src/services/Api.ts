import { injectable } from 'inversify';
import axios from 'axios';
import Article from '@/models/Article';
import AnswerStat from '@/models/AnswerStat';
import InstanceStats from '@/models/InstanceStats';
import Stats from '@/models/Stats';

@injectable()
export default class {
    getStats(): Promise<InstanceStats> {
        return axios.get(process.env.VUE_APP_API + 'articles/stats')
            .then(response => response.data)
            .then(response => {
                const instanceStats = new InstanceStats();

                instanceStats.count = {
                    ...response,
                    total: response.real + response.fake,
                };

                return instanceStats;
            })
        ;
    }

    getArticleFromUrl(url: string): Promise<Article> {
        const params = { url };

        return axios.get(process.env.VUE_APP_API + 'articles/from-url', { params })
            .then(response => response.data)
        ;
    }

    getArticles(): Promise<Article[]> {
        return axios.get(process.env.VUE_APP_API + 'articles')
            .then(response => response.data)
        ;
    }

    getArticlesExport(): Promise<Article[]> {
        return axios.get(process.env.VUE_APP_API + 'articles/export')
            .then(response => response.data)
        ;
    }

    getWhichOneIsFakeArticles(exceptIds?: string): Promise<Article[]> {
        const params: {exceptIds?: string} = {};

        if (exceptIds) {
            params.exceptIds = exceptIds;
        }

        return axios.get(process.env.VUE_APP_API + 'articles/which-one-is-fake', { params })
            .then(response => response.data)
        ;
    }

    getCustomWhichOneIsFakeArticles(ids: string): Promise<Article[]> {
        return axios.get(process.env.VUE_APP_API + 'articles/which-one-is-fake/' + ids)
            .then(response => response.data)
        ;
    }

    postArticle(article: Article): Promise<void> {
        return axios.post(process.env.VUE_APP_API + 'articles', {
            ...article,
        })
            .then(response => response.data)
        ;
    }

    getAnswerStats(): Promise<Stats> {
        return axios.get(process.env.VUE_APP_API + 'answer-stats')
            .then(response => response.data)
        ;
    }

    postAnswerStat(answerStat: AnswerStat): Promise<void> {
        return axios.post(process.env.VUE_APP_API + 'answer-stats', {
            ...answerStat,
        })
            .then(response => response.data)
        ;
    }
}
