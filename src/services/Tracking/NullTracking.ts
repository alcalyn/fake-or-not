import { injectable } from 'inversify';
import TrackingInterface from '@/services/Tracking/TrackingInterface';

@injectable()
export default class implements TrackingInterface {
    playAgain(): void {
    }

    submitArticle(): void {
    }

    exportArticles(): void {
    }
}
