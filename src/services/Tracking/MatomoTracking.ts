import { injectable } from 'inversify';
import { Vue } from 'vue-property-decorator';
import TrackingInterface from '@/services/Tracking/TrackingInterface';

@injectable()
export default class implements TrackingInterface {
    playAgain(): void {
        Vue.prototype.$matomo.trackEvent('play', 'again');
    }

    submitArticle(): void {
        Vue.prototype.$matomo.trackEvent('submit-article', 'submitted');
    }

    exportArticles(): void {
        Vue.prototype.$matomo.trackEvent('export', 'export-articles');
    }
}
