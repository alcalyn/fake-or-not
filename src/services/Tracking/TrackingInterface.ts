export default interface TrackingInterface {
    playAgain(): void;
    submitArticle(): void;
    exportArticles(): void;
}
