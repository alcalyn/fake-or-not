import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import GorafiOrNot from '@/views/GorafiOrNot.vue';

describe('GorafiOrNot.vue', () => {
    it('displays list of article titles', () => {
        const wrapper = shallowMount(GorafiOrNot);
        expect(wrapper.text()).to.be.not.undefined;
    });
});
